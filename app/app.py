import json
import logging
import sys
import pandas as pd
import pickle
from functools import wraps
from logging.config import dictConfig

from flask import (
    Flask, 
    render_template, 
    jsonify, 
    request,
    redirect,
    url_for,
    session,
    g
)
from flask_mail import Mail, Message

from flask_oauthlib.client import OAuth

import models as m
import sampling as spl
from decorators import async

NAME_TABLE = pickle.load(open('../model/name_table.pickle', 'rb'))
app = Flask(__name__, static_url_path="", static_folder="static")

ext_config = json.loads(open('config.json').read())
app.secret_key = ext_config['APP']['SECRET_KEY']

app.debug = ext_config['DEBUG']
app.config['REQUIREAUTH'] = ext_config['REQUIREAUTH']

# LOGGING
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
app.logger.addHandler(handler)
if app.debug:
    app.logger.setLevel(logging.DEBUG)
    
# EMAIL
app.config.update(
    MAIL_SERVER=ext_config['MAIL']['MAIL_SERVER'],
    MAIL_PORT=ext_config['MAIL']['MAIL_PORT'],
    MAIL_USE_SSL=ext_config['MAIL']['MAIL_USE_SSL'],
    MAIL_USERNAME=ext_config['MAIL']['MAIL_USERNAME'],
    MAIL_PASSWORD=ext_config['MAIL']['MAIL_PASSWORD'],
    SEND_EMAILS=ext_config['MAIL']['SENDEMAILS']
)

mail=Mail(app)

# GOOGLE OAUTH Config
app.config['GOOGLE_ID'] = ext_config['OAUTH']['GOOGLE']['ID']
app.config['GOOGLE_SECRET'] = ext_config['OAUTH']['GOOGLE']['SECRET']

# FACEBOOK OAUTH Config
app.config['FACEBOOK_APP_ID'] = ext_config['OAUTH']['FACEBOOK']['ID']
app.config['FACEBOOK_APP_SECRET'] = ext_config['OAUTH']['FACEBOOK']['SECRET']

oauth = OAuth(app)

google = oauth.remote_app(
    'google',
    consumer_key=app.config.get('GOOGLE_ID'),
    consumer_secret=app.config.get('GOOGLE_SECRET'),
    request_token_params={
        'scope': 'email'
    },
    base_url='https://www.googleapis.com/oauth2/v1/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    authorize_url='https://accounts.google.com/o/oauth2/auth',
)

facebook = oauth.remote_app(
    'facebook',
    consumer_key=app.config.get('FACEBOOK_APP_ID'),
    consumer_secret=app.config.get('FACEBOOK_APP_SECRET'),
    request_token_params={'scope': 'email'},
    base_url='https://graph.facebook.com',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    access_token_method='GET',
    authorize_url='https://www.facebook.com/dialog/oauth'
)

@async
def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email(to, subject, message=None, html=None):
    if not app.config['SEND_EMAILS']:
        return

    if message is None and html is None:
        raise ValueError("You must provide a value for message, html or both.")
    
    mail_message = Message(
        subject,
        sender=ext_config['MAIL']['SENDER'],
        recipients=[to,]        
    )
    mail_message.body = message
    mail_message.html = html

    send_async_email(app, mail_message)

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None:
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

@app.before_request
def load_user():
    email = session.get('email', None)
    g.user = m.User.find_by_email(email)

@app.route("/")
@login_required
def index():
    return render_template("index.bootstrap.html")

@app.route("/tutorial")
@login_required
def tutorial():
    return render_template("tutorial.bootstrap.html")

@app.route("/results")
@login_required
def results():
    return render_template("results.bootstrap.html")

@app.route("/settings")
@login_required
def settings():
    sent_invitation = m.PartnerInvitation.find_by_user(g.user)
    received_invitation = m.PartnerInvitation.find_open_by_partner(g.user)
    user_settings = m.UserSettings.find_by_user(g.user)

    # determine form elements to show
    show_invite_form = not received_invitation and not sent_invitation
    show_accept_form = received_invitation and received_invitation.is_open()
    show_revoke_form_pending = sent_invitation and sent_invitation.is_open()
    show_revoke_form_accepted = sent_invitation and sent_invitation.is_accepted()

    partner = g.user.approved_partner()
    partner_full_name = None
    if partner:
        partner_full_name = partner.full_name()

    return render_template(
        "settings.bootstrap.html",
        sent_invitation=sent_invitation,
        received_invitation=received_invitation,
        user_settings=user_settings,
        show_invite_form=show_invite_form,
        show_accept_form=show_accept_form,
        show_revoke_form_accepted=show_revoke_form_accepted,
        show_revoke_form_pending=show_revoke_form_pending,
        partner_full_name=partner_full_name,
    )

@app.route("/api/settings/save/name_filter", methods=['POST'])
@login_required
def save_name_filter():
    preference = request.form.get('name_filter', 3)
    m.UserSettings.set_filter_state(g.user, preference)
    
    return jsonify({'status': 'success',
                    'message': 'Name filter set to ' + m.UserSettings.filter_state_to_string(preference) + '.',
                    'preference': preference})

@app.route("/about")
def about():
    return render_template("about.bootstrap.html")

@app.route("/api/like", methods=['POST'])
@login_required
def api_like():
    name_id = request.form.get('id', None)
    if name_id:
        user = g.user
        m.UserOpinion.user_like(name_id, user.id)
    
    return jsonify({'status': 'success'})

@app.route("/api/dislike", methods=['POST'])
@login_required
def api_dislike():
    name_id = request.form.get('id', None)
    if name_id:
        user = g.user
        m.UserOpinion.user_dislike(name_id, user.id)

    return jsonify({'status': 'success'})

@app.route("/api/name", methods=['GET'])
@login_required
def api_get_name():
    user = g.user
    name, similar = spl.sample_name(user, NAME_TABLE)
    result = name.to_dict()
    result['similar'] = similar
    return jsonify(result)

@app.route("/api/results", methods=['GET'])
@login_required
def api_results():
    user = g.user
    return jsonify({
        'stats': m.UserOpinion.user_stats(user),
        'results': m.UserOpinion.user_likes(user, as_dict=True),
        'shared': m.UserOpinion.shared_likes(user)
    })

@app.route("/api/reset", methods=['POST'])
@login_required
def api_reset():
    user = g.user
    delete_count = m.UserOpinion.reset(user)
    return jsonify({
        'status': 'success',
        'message': 'Your account has been reset.',
        'deleted': delete_count
    })

@app.route("/api/partner/invite", methods=['POST'])
@login_required
def invite_partner():
    user = g.user
    partner_email = request.form.get('partner_email', None)
    
    if partner_email:
        invite = m.PartnerInvitation.create_invitation(user, partner_email)
        mail_html = render_template(
            'mail/invite.html',
            invitee=user.full_name()
        )
        send_email(partner_email, 'Invitation', html=mail_html)
        return jsonify({
            'status': 'success',
            'message': 'You have successfully invited {}.'.format(partner_email),
            'invite_id': invite.id,
            'partner_text': partner_email,           
        })
        
    return jsonify({
        'status': 'error',
        'message': 'Partner email must be provided.',
    })

@app.route("/api/partner/revoke", methods=['POST'])
@login_required
def revoke_partner():
    user = g.user
    invite_id = request.form.get('invite_id', None)
    
    if not invite_id:
        return jsonfiy({
            'status': 'error',
            'message': 'No invitation provided.'
        })
    
    pi = m.PartnerInvitation.find_by_invite_and_user(invite_id, user)
    partner_email = pi.partner_email
    app.logger.debug('REvoking partner invite id {}, user {}'.format(invite_id, user.full_name()))
    if not pi:
        return jsonify({
            'status': 'error',
            'message': 'Unable to find invitation.'
        })
    
    # revoke user -> partner sharing
    pi.revoke_invitation()
    
    # revoke partner -> user sharing
    partner = m.User.find_by_email(pi.partner_email)
    partner_pi = m.PartnerInvitation.find_by_user_and_partner(partner, user)

    if partner_pi:
        app.logger.debug('Revoking partner invite user {}, partner {}, invite id {}'.format(
            user.full_name(), partner.full_name(), partner_pi.id))
        partner_pi.revoke_invitation()
        
    # delete the relationship
    result = m.UserPartner.remove_partnership(user, partner)
    app.logger.debug('Remove user partner relations success:'.format(str(result)))

    mail_html = render_template(
        'mail/invite.revoked.html',
        invitee=user.full_name()
    )
    send_email(partner_email, 'Invitation - Revoked', html=mail_html)

    return jsonify({
        'status': 'success',
        'message': 'You are no longer sharing information.'
    })

@app.route("/api/invite/accept", methods=['POST'])
@login_required
def accept_invite():
    user = g.user
    invite_id = request.form.get('invite_id', None)
    
    if not invite_id:
        return jsonfiy({
            'status': 'error',
            'message': 'No invitation provided.'
        })
    partner_email = user.email
    pi = m.PartnerInvitation.find_by_invite_and_partner_email(invite_id, partner_email)
    if not pi:
        return jsonify({
            'status': 'error',
            'message': 'Unable to find invitation.'
        })
    
    # accept invite and add user partner relation
    pi.accept_invitation()
    up = m.UserPartner()
    up.user = pi.user
    up.partner = user
    up.save()
    
    # invite the invitee and auto accept for them
    originator_email = pi.user.email
    originator_name = pi.user.full_name()
    pi = m.PartnerInvitation.create_invitation(user, originator_email)
    pi.accept_invitation()
    up2 = m.UserPartner()
    up2.user = user
    up2.partner = up.user
    up2.save()

    mail_html = render_template(
        'mail/invite.accept.html',
        invitee=user.full_name()        
    )
    send_email(originator_email, 'Invitation - Accepted', html=mail_html)
    
    return jsonify({
        'status': 'success',
        'message': 'You and {} are now sharing results with each other.'.format(originator_name),
        'partner_text': originator_name,
        'invite_id': pi.id,
    })

@app.route("/api/invite/decline", methods=['POST'])
@login_required
def decline_invite():
    user = g.user
    invite_id = request.form.get('invite_id', None)
    
    if not invite_id:
        return jsonfiy({
            'status': 'error',
            'message': 'No invitation provided.'
        })
    
    pi = m.PartnerInvitation.find_by_invite_and_partner_email(invite_id, user.email)
    if not pi:
        return jsonify({
            'status': 'error',
            'message': 'Unable to find invitation.'
        })
    
    pi.decline_invitation()
    
    mail_html = render_template(
        'mail/invite.declined.html',
        invitee=user.full_name()
    )
    send_email(pi.user.email, 'Invitation - Declined', html=mail_html)

    return jsonify({
        'status': 'success',
        'message': 'You have successfully declined the invitation.'
    })

# OAuth Authentication Facebook, Google, etc...
@app.route('/login')
def login():
    oauth_token = session.get('oauth_token')
    
    if 'oauth_token' in session:
        oauth_provider = session.get('oauth_provider')
        
        if oauth_provider == 'dev':
            session['email'] = 'dev@nowhere.com'
        else:        
            if oauth_provider == 'google':
                me = google.get('userinfo')            
            else:
                me = facebook.get('/me')

            session['email'] = me.data['email']
        
        next_uri = request.args.get('next')
        if next_uri:
            return redirect(next_uri)

        return redirect(url_for('index'))

    return render_template('login.html', requireauth=app.config['REQUIREAUTH'])

def login_redirect_uri(user_email):
    """
    Helper method to determine what URL to route the user to. On
    first login, the user is redirected to the Tutorial page. On
    second login, the user is redirected to the Index page.
    """
    app.logger.debug('user email is {}'.format(user_email))
    redirect_uri = 'index'
    if not m.User.find_by_email(user_email):
        app.logger.debug('user email not found')
        redirect_uri = 'tutorial'

    app.logger.debug('redirect uri is {}'.format(redirect_uri))

    return redirect_uri

# Developer setting - No Authentication
@app.route('/login/noauth')
def noauth_login():
    redirect_uri = login_redirect_uri('dev@nowhere.com')
    m.User.noauth_login()
    session['oauth_token'] = 'dev'
    session['oauth_provider'] = 'dev'
    return redirect(url_for(redirect_uri))

# GOOGLE
@app.route('/login/google')
def google_login():
    return google.authorize(callback=url_for('google_authorized', _external=True))

@app.route('/login/google/authorized')
def google_authorized():
    resp = google.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    token = (resp['access_token'], '')
    provider = 'google'
    session['google_token'] = token
    session['oauth_token'] = token
    session['oauth_provider'] = 'google'
    me = google.get('userinfo')

    redirect_uri = login_redirect_uri(me.data['email'])
    m.User.oauth_login(token, provider, me.data)
    
    return redirect(url_for(redirect_uri))

@google.tokengetter
def get_google_oauth_token():
    return session.get('google_token')

# FACEBOOK
@app.route('/login/facebook')
def facebook_login():
    callback = url_for(
        'facebook_authorized',
        next=request.args.get('next') or request.referrer or None,
        _external=True
    )
    return facebook.authorize(callback=callback)

# FIXME: Facebook Oauth only partially works as they do not always
# send the user's email address. I wanted to tie all of the user
# logins to their email, but this will require a post step to ask
# the user for their email address and model changes for the user.
@app.route('/login/facebook/authorized')
def facebook_authorized():
    resp = facebook.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )

    session['oauth_token'] = (resp['access_token'], '')
    me = facebook.get('/me')
    for k, v in me.__dict__.items():
        print(k, v)
    session['oauth_provider'] = 'facebook'
    session['email'] = me.data['email']
    
    m.User.oauth_login(session['oauth_token'], session['oauth_provider'], me.data)
    return redirect(url_for(login_redirect_uri(me.data['email'])))

def facebook_email():
    pass
    
@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')

# LOGOUT ALL
@app.route('/logout')
@login_required
def logout():
    session.clear()
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)