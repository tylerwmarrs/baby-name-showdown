import json

#from peewee import PostgresqlDatabase
from playhouse.pool import PooledPostgresqlExtDatabase
from playhouse.sqlite_ext import SqliteExtDatabase

# config = json.loads(open('config.json').read())

DB = SqliteExtDatabase('my_app.db')

# DB = PooledPostgresqlExtDatabase(
#     config['DB']['database'],
#     user=config['DB']['user'],
#     password=config['DB']['password'],
#     host=config['DB']['host'],
#     port=config['DB']['port'],
#     autocommit=True,
#     autorollback=True
# )