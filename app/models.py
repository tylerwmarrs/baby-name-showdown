import random
from enum import Enum

from peewee import *

import sampling as spl
import db_settings
import my_logger

DB = db_settings.DB

logger = my_logger.get_logger_instance(__name__)

class BaseModel(Model):
    """
    Base model used to set the database to use.
    """
    class Meta:
        database = db_settings.DB

class User(BaseModel):
    """
    User model for authentication and to tie opinions about names
    to a specific person.
    """
    email = CharField(index=True)
    family_name = CharField()
    given_name = CharField()
    # app_id = CharField(index=True)
    oauth_token = CharField(index=True)
    oauth_provider = CharField(index=True)
    
    @staticmethod
    def find_by_email(email):
        user = None
        try:
            user = User.get(User.email == email)
        except User.DoesNotExist:
            pass
        
        return user
    
    @staticmethod
    def noauth_login():
        user = None
        try:
            user = User.get(User.email == 'dev@nowhere.com')
            user.oauth_token = 'dev'
            user.oauth_provider = 'dev'
            user.save()
        except User.DoesNotExist:
            user = User(
                email='dev@nowhere.com',
                family_name='Dev',
                given_name='Dev',
                oauth_token='dev',
                oauth_provider='dev'
            )
            user.save()
            
        return user
    
    @staticmethod
    def oauth_login(token, provider, data):
        # try to find user by email or create it
        user = None
        try:
            user = User.get(User.email == data['email'])
            user.oauth_token = token
            user.oauth_provider = provider
            user.save()
        except User.DoesNotExist:
            user = User(
                email=data['email'],
                family_name='',
                given_name=data['email'],
                oauth_token=token,
                oauth_provider=provider
            )
            user.save()
            
        return user
    
    def full_name(self):
        return self.given_name + " " + self.family_name
    
    def approved_partner(self):        
        sql = """
        SELECT up.*
        FROM userpartner up
        JOIN partnerinvitation pi
        ON up.user_id = pi.user_id
        and pi.state = {}
        and up.user_id = {}
        """
        accepted = InvitationStatus.ACCEPTED.value
        query = UserPartner.raw(sql.format(str(accepted), str(self.id)))
        partners = []
        for result in query:
            partners.append(result.partner)

        if len(partners) > 0:
            return partners[0]
        return None

class NameFilterState(Enum):
    """
    Enumerable for UserSettings.filter_state
    """
    BOY_NAMES_ONLY = 1
    GIRL_NAMES_ONLY = 2
    ANY_NAME = 3

class UserSettings(BaseModel):
    """
    Keeps track of user settings.
    """
    user = ForeignKeyField(User, related_name='user_settings')
    filter_state = IntegerField()
    
    @staticmethod
    def find_by_user(user):
        us = None
        try:
            us = UserSettings.get(
                (UserSettings.user == user)
            )
        except DoesNotExist:
            us = UserSettings()
            us.filter_state = NameFilterState.ANY_NAME.value
            us.user = user
        
        return us
    
    @staticmethod
    def set_filter_state(user, state):
        user_settings = UserSettings.find_by_user(user)
        
        if not user_settings:
            user_settings = UserSettings()
            user_settings.user = user
        
        user_settings.filter_state = int(state)
        user_settings.save()
    
    @staticmethod
    def filter_state_to_string(state):
        filter_name = NameFilterState(int(state)).name
        return filter_name.lower().replace('_', ' ')
        

class UserPartner(BaseModel):
    """
    Keeps track of partners associated with the user. Typically
    only one partner will exist.
    """
    user = ForeignKeyField(User, related_name='user_partner_users')
    partner = ForeignKeyField(User, related_name='partners')
    
    @staticmethod
    @DB.atomic()
    def remove_partnership(user, partner):
        if not user or not partner:
            return True
        
        q = UserPartner.delete().where(
            (UserPartner.user == user) & (UserPartner.partner == partner)
        )
        
        q_result = q.execute()
        logger.debug('Deleting user {} -> partner {}'.format(user.full_name(), partner.full_name()))
        logger.debug('Deleted records {}'.format(q_result))
        
        q2 = UserPartner.delete().where(
            (UserPartner.user == partner) & (UserPartner.partner == user)
        )
        
        q2_result = q2.execute()
        logger.debug('Deleting partner {} -> user {}'.format(partner.full_name(), user.full_name()))
        logger.debug('Deleted records {}'.format(q2_result))
        
        return q_result == 1 and q2_result == 1

class InvitationStatus(Enum):
    """
    Enumerable for PartnerInvitation.state
    """
    OPEN = 1
    ACCEPTED = 2
    DECLINED = 3
    REVOKED = 4

class PartnerInvitation(BaseModel):
    user = ForeignKeyField(User, related_name='partner_invite_users')
    partner_email = CharField()
    state = IntegerField()
    
    @DB.atomic()
    def decline_invitation(self):
        self.delete_instance()
    
    @DB.atomic()
    def accept_invitation(self):
        self.state = InvitationStatus.ACCEPTED.value
        self.save()
    
    @DB.atomic()
    def revoke_invitation(self):
        self.delete_instance()
    
    @staticmethod
    @DB.atomic()    
    def create_invitation(user, partner_email):
        invite = PartnerInvitation()
        invite.user = user
        invite.partner_email = partner_email
        invite.state = InvitationStatus.OPEN.value
        invite.save()
        
        return invite
    
    @staticmethod
    def find_by_invite_and_partner_email(invite_id, partner_email):
        pi = None
        try:
            pi = PartnerInvitation.get(
                (PartnerInvitation.id == invite_id) & (PartnerInvitation.partner_email == partner_email)
            )
        except DoesNotExist:
            pass
        
        return pi
    
    @staticmethod
    def find_by_invite_and_user(invite_id, user):
        pi = None
        try:
            pi = PartnerInvitation.get(
                (PartnerInvitation.id == invite_id) & (PartnerInvitation.user == user)
            )
        except DoesNotExist:
            pass
        
        return pi
    
    @staticmethod
    def find_by_user_and_partner(user, partner):
        pi = None
        try:
            pi = PartnerInvitation.get(
                (PartnerInvitation.user == user) & (PartnerInvitation.partner_email == partner.email)
            )
        except DoesNotExist:
            pass
        
        return pi
    
    @staticmethod
    def find_by_user(user):
        pi = None
        try:
            pi = PartnerInvitation.get(
                (PartnerInvitation.user == user) & (PartnerInvitation.state != InvitationStatus.REVOKED.value)
            )
        except DoesNotExist:
            pass
        
        return pi
    
    @staticmethod
    def find_open_by_partner(partner):
        pi = None
        try:
            pi = PartnerInvitation.get(
                (PartnerInvitation.partner_email == partner.email) & (PartnerInvitation.state == InvitationStatus.OPEN.value)
            )
        except DoesNotExist:
            pass
        
        return pi
    
    def is_open(self):
        return self.state == InvitationStatus.OPEN.value
    
    def is_accepted(self):
        return self.state == InvitationStatus.ACCEPTED.value
    
    def is_declined(self):
        return self.state == InvitationStatus.DECLINED.value
    
    def is_revoked(self):
        return self.state == InvitationStatus.REVOKED.value

class Name(BaseModel):
    """
    The name itself with some meta-data used for clustering.
    """
    name = CharField(index=True)
    length = IntegerField()
    name_percentage_male = DoubleField(index=True)
    name_percentage_female = DoubleField(index=True)
    popularity_percentage = DoubleField(index=True)
    metaphone_code = IntegerField()
    starts_with_code = IntegerField()
    ends_with_code = IntegerField()
    
    @staticmethod
    def by_name(name):
        return Name.get(Name.name == name)

    @staticmethod
    def total_names():
        return Name.select().count()

    @staticmethod
    def total_boy_names():
        return Name.select().where(Name.name_percentage_male > 0).count()

    @staticmethod
    def total_girl_names():
        return Name.select().where(Name.name_percentage_female > 0).count()
    
    def to_dict(self, full=False):
        result = {
            'id': self.id,
            'name': self.name,
            'name_percentage_male': self.name_percentage_male,
            'name_percentage_female': self.name_percentage_female,
            'popularity_percentage': self.popularity_percentage,
        }
        
        if full:
            rest = {
                'length': self.length,
                'metaphone_code': self.metaphone_code,
                'starts_with_code': self.starts_with_code,
                'ends_with_code': self.ends_with_code,
                'cluster': self.cluster
            }
            result = {**result, **rest}

        return result


class UserOpinion(BaseModel):
    """
    The user's opinions on each name that they have been
    presented.
    """
    user = ForeignKeyField(User, related_name='users')
    name = ForeignKeyField(Name, related_name='names')
    likes = IntegerField(index=True)
    dislikes = IntegerField()
    
    @staticmethod
    def get_opinion(name, user):
        try:
            return UserOpinion.get(
                (UserOpinion.name == name) & (UserOpinion.user == user)
            )
        except DoesNotExist:
            return None
    
    @staticmethod
    @DB.atomic()
    def user_like(name, user):
        result = UserOpinion.get_opinion(name, user)
        if result:
            result.likes += 1
        else:
            result = UserOpinion(
                name=name,
                user=user,
                likes=1,
                dislikes=0
            )

        result.save()
    
    @staticmethod
    @DB.atomic()
    def user_dislike(name, user):
        result = UserOpinion.get_opinion(name, user)
        if result:
            result.dislikes += 1
        else:
            result = UserOpinion(
                name=name,
                user=user,
                likes=0,
                dislikes=1
            )

        result.save()
        
    @staticmethod
    def user_likes(user, as_dict=False):
        query = UserOpinion.select().join(Name).where(
            (UserOpinion.user == user) & (UserOpinion.likes > 0)
        ).order_by(UserOpinion.likes.desc(), Name.name)
        
        results = []
        for result in query:
            if as_dict:
                results.append(result.to_dict())
            else:
                results.append(result)
                
        return results
    
    @staticmethod
    def user_dislikes(user, as_dict=False):
        query = UserOpinion.select().join(Name).where(
            (UserOpinion.user == user) & (UserOpinion.dislikes > 0)
        ).order_by(UserOpinion.dislikes.desc())
        
        results = []
        for result in query:
            if as_dict:
                results.append(result.to_dict())
            else:
                results.append(result)
                
        return results
    
    @staticmethod
    def user_like_count(user):
        return UserOpinion.select().where(
            (UserOpinion.user == user) & (UserOpinion.likes > 0)
        ).count()

    @staticmethod
    def user_boy_like_count(user):
        result = UserOpinion.raw("""
            SELECT COUNT(1) as count
            FROM useropinion up
            JOIN name n
            ON up.name_id = n.id
            WHERE up.user_id = {}
            AND likes > 0
            AND dislikes < 2
            AND n.name_percentage_male > 0
        """.format(user.id)
        ).execute()

        count = 0
        for row in result:
            count = row.count
            break

        return count

    @staticmethod
    def user_girl_like_count(user):
        result = UserOpinion.raw("""
            SELECT COUNT(1) as count
            FROM useropinion up
            JOIN name n
            ON up.name_id = n.id
            WHERE up.user_id = {}
            AND likes > 0
            AND dislikes < 2
            AND n.name_percentage_female > 0
        """.format(user.id)
        ).execute()

        count = 0
        for row in result:
            count = row.count
            break

        return count
    
    @staticmethod
    def user_dislike_count(user):
        return UserOpinion.select().where(
            (UserOpinion.user == user) & (UserOpinion.dislikes > 0)
        ).count()
    
    @staticmethod
    def user_total_count(user):
        return UserOpinion.select().where(
            (UserOpinion.user == user)
        ).count()
    
    @staticmethod
    def user_stats(user):
        return {
            'likes': UserOpinion.user_like_count(user),
            'dislikes': UserOpinion.user_dislike_count(user),
            'total': UserOpinion.user_total_count(user)
        }
    
    @staticmethod
    @DB.atomic()    
    def reset(user):
        """
        Deletes all UserOpinion's for a given user.
        
        Returns the number of records deleted.
        """
        query = UserOpinion.delete().where(UserOpinion.user == user)
        return query.execute()
    
    @staticmethod
    def shared_likes(user):
        partner = user.approved_partner()
        if not partner:
            return []
        
        cursor = DB.execute_sql("""
        SELECT n.name,
        n.name_percentage_male,
        n.name_percentage_female,
        n.popularity_percentage,
        mine.likes as my_likes, 
        theirs.likes as their_likes
        FROM useropinion as mine
        JOIN useropinion as theirs
        ON mine.name_id = theirs.name_id
        AND mine.likes > 0
        AND mine.dislikes < 2
        AND theirs.likes > 0
        AND theirs.dislikes < 2
        AND mine.user_id = {}
        and theirs.user_id = {}
        JOIN name as n
        ON mine.name_id = n.id
        """.format(str(user.id), str(partner.id))
        )
        
        column_names = [desc[0] for desc in cursor.description]
        results = []
        for row in cursor:
            result = {}
            for col, value in zip(column_names, row):
                result[col] = value

            results.append(result)
            
        return results
    
    def to_dict(self):
        return {
            'name': self.name.to_dict(),
            'likes': self.likes,
            'dislikes': self.dislikes
        }