$(document).ready(function(){
    var clear_table = function() {
        $('#results-table tbody').html('');  
    };
    
    var clear_shared_table = function() {
        $('#shared-table tbody').html('');
    };
    
    var append_html = function(html) {
        $('#results-table tbody').append(html);
    };
    
    var append_shared_html = function(html) {
        $('#shared-table tbody').append(html);
    };
    
    var no_results_stats = function() {
        var html = '<p><strong>No stats to display yet.</strong></p>';
        $('#results-stats').html(html);
    };
    
    var no_results = function() {
        var html = '<tr><td colspan="6" align="center"><strong>No Results Yet!</strong></td></tr>';
        append_html(html);
    };
    
    var no_results_shared = function() {
        var html = '<tr><td colspan="6" align="center"><strong>No Results Yet!</strong></td></tr>';
        append_shared_html(html);
    };
    
    var append_stats = function(stats) {
        var html = '<table class="table table-sm">';
        html += '<tr><td><strong>Names viewed</strong></td>';
        html += '<td>' + stats.total.toLocaleString() + '</td></tr>';
        html += '<tr><td><strong>Likes</strong></td>';
        html += '<td>' + stats.likes.toLocaleString() + '</td></tr>';
        html += '<tr><td><strong>Dislikes</strong></td>';
        html += '<td>' + stats.dislikes.toLocaleString() + '</td></tr>';
        html += '</table>';
        $('#results-stats').html(html);
    };
    
    var pretty_round = function (number) {
        number = number.toPrecision(4);
        
        if (number == 100) {
            number = 100;
        }
        
        if (number < 0.0001) {
            number = 0;
        }
        return number;
    };
    
    var append_result = function(data) {
        var html = '<tr>';
        html += '<td>' + data.name.name + '</td>';
        html += '<td>' + data.likes + '</td>';
        html += '<td>' + data.dislikes + '</td>';
        html += '<td>' + pretty_round(data.name.name_percentage_male) + '</td>';
        html += '<td>' + pretty_round(data.name.name_percentage_female) + '</td>';
        //html += '<td>' + pretty_round(data.name.popularity_percentage) + '</td>';
        html += '</tr>';
        append_html(html);
    };
    
    var append_shared_result = function(data) {
        var html = '<tr>';
        html += '<td>' + data.name + '</td>';
        html += '<td>' + data.my_likes + '</td>';
        html += '<td>' + data.their_likes + '</td>';
        html += '<td>' + pretty_round(data.name_percentage_male) + '</td>';
        html += '<td>' + pretty_round(data.name_percentage_female) + '</td>';
        html += '</tr>';
        append_shared_html(html);
    };
    
    var fetch_results = function() {
        $.getJSON( "/api/results", function(data) {
            clear_table();
            clear_shared_table();
            if (data.results.length < 1) {
                no_results();
                no_results_stats();
            } else {
                append_stats(data.stats);
                $.each(data.results, function (index, row) {
                    append_result(row);
                });
                
                if (data.shared.length > 0) {
                    $.each(data.shared, function(index, row){
                        append_shared_result(row);
                    });
                } else {
                    no_results_shared();
                }
            }
        });
    };
    
    fetch_results();
});