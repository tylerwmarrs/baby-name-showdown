$(document).ready(function(){
    var fetch_name = function(swiper) {   
        $.getJSON( "/api/name", function( data ) {
            var icon_html = '';
            if (data.name_percentage_male > 0 && data.name_percentage_female > 0) {
                icon_html = '<div style="display:block;"><img src="/img/gender-icons/male.png" /><img src="/img/gender-icons/female.png" /></div>';
            } else if (data.name_percentage_male > 0) {
                icon_html = '<div style="display:block;"><img src="/img/gender-icons/male.png" /></div>';
            } else if (data.name_percentage_female > 0) {
                icon_html = '<div style="display:block;"><img src="/img/gender-icons/female.png" /></div>';;
            }
            
            var html = icon_html + '<div class="name">' + data.name + '</div>';
            $('#baby-name').html(html);
            $('#baby-name').data('name', data.name);
            $('#baby-name').data('id', data.id);
        });
    };

    var animate_dislike = function() {
        $('#baby-name-container').animate({
            opacity: .75,
            duration: 0,
            'backgroundColor': '#F2DEDE',
            'marginLeft': "-=15px",
            'marginRight': "+=30px"
        });
        $('#baby-name-container').animate({
            opacity: 1,
            duration: 0,
            'backgroundColor': '#FFFFFF',
            'marginLeft' : "+=15px",
            'marginRight' : "-=30px"
        });
    };

    var animate_like = function() {
        $('#baby-name-container').animate({
            opacity: .75,
            duration: 0,
            'backgroundColor': '#DFF0D8',                        
            'marginLeft': "+=30px",
            'marginRight': "-=15px"
        });
        $('#baby-name-container').animate({
            opacity: 1,
            duration: 0,
            'backgroundColor': '#FFFFFF',
            'marginLeft' : "-=30px",
            'marginRight' : "+=15px"
        });
    };
    
    var post_like = function() {
        var name = $('#baby-name').data('name');
        var id = $('#baby-name').data('id');
        if (id) {
            $.post("/api/like", {id: id, name: name})
            .done(function( data ) {
                animate_like();
                fetch_name();
            });
        }
    };
    
    var post_dislike = function() {
        var name = $('#baby-name').data('name');
        var id = $('#baby-name').data('id');
        if (id) {
            $.post("/api/dislike", {id: id, name: name})
            .done(function( data ) {
                animate_dislike();
                fetch_name();
            });
        }
    };
    
    fetch_name();
    $('div.like-button').on('click', function() {
        post_like();
    });
    $('div.dislike-button').on('click', function() {
        post_dislike();
    });
    $("body").keydown(function(e){
        // left arrow
        if ((e.keyCode || e.which) == 37) {   
            post_dislike();
        }
        // right arrow
        if ((e.keyCode || e.which) == 39) {
            post_like();
        }   
    });

    $("#baby-name-container").swipe( {
        //Generic swipe handler for all directions
        swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
            post_dislike();            
        },
        swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
            post_like();
        },
        //Default is 75px, set to 0 for demo so any distance triggers swipe
         threshold:100
    });

});