function notify(status, message) {
    if (status == 'success') {
        $('.alert').addClass('alert-success');
        message = '<strong>Success!</strong> ' + message;
    } else if (status == 'error') {
        $('.alert').addClass('alert-danger');
        message = '<strong>Error:</strong> ' + message;
    }
    
    $('.alert #message-text').html(message);
    
    $('.alert').fadeTo(4000, 500).slideUp(500, function(){
        $('.alert').slideUp(500);
    });
};