$(document).ready(function(){
    var hide_all_invite_forms = function() {
        $("#invite-form").hide();
        $("#accept-form").hide();
        $("#revoke-form-pending").hide();
        $("#revoke-form-accepted").hide();
    };

    var reset_all_invite_ids = function() {
        $("input[name=invite_id]").val("");
    };

    var reset_all_partner_texts = function() {
        $("#partner-text").val("");
        $("#partner-email").val("");
    };

    var reset_invite_forms = function() {
        hide_all_invite_forms();
        reset_all_invite_ids();
        reset_all_partner_texts();
    };

    $('#invite-form form').submit(function(event){
        $.post('/api/partner/invite', $(this).serialize(), function(data){
            notify(data.status, data.message);

            if (data.status == 'success') {
                reset_invite_forms();                
                $("#revoke-form-pending").show();
                $("#revoke-form-pending input[name=invite_id]").val(data.invite_id);
                $("#revoke-form-pending #partner-text").val(data.partner_text + " (Pending)");
            }
        });
        event.preventDefault();
    });
    $('#accept-form button[value=accept]').on('click', function(event){
        var invite_id = $('#accept-form input[name=invite_id]').val();
        $.post('/api/invite/accept', {"invite_id": invite_id}, function(data){
            notify(data.status, data.message);            
            if (data.status == 'success') {
                reset_invite_forms();
                $("#revoke-form-accepted").show();
                $("#revoke-form-accepted input[name=invite_id]").val(data.invite_id);
                $("#revoke-form-accepted #partner-text").val(data.partner_text  + " (Accepted)");
            }
        });
        event.preventDefault();
    });
    $('#accept-form button[value=decline]').on('click', function(event){
        var invite_id = $('#accept-form input[name=invite_id]').val();
        $.post('/api/invite/decline', {"invite_id": invite_id}, function(data){
            notify(data.status, data.message);
            if (data.status == 'success') {
                reset_invite_forms();
                $("#invite-form").show();
            }
        });
        event.preventDefault();
    });
    $('#revoke-form-pending form').submit(function(event){
        $.post('/api/partner/revoke', $(this).serialize(), function(data){
            notify(data.status, data.message);
            if (data.status == 'success') {
                reset_invite_forms();
                $("#invite-form").show();
            }
        });
        event.preventDefault();
    });
    $('#revoke-form-accepted form').submit(function(event){
        $.post('/api/partner/revoke', $(this).serialize(), function(data){
            notify(data.status, data.message);
            if (data.status == 'success') {
                reset_invite_forms();
                $("#invite-form").show();
            }
        });
        event.preventDefault();
    });
    $('#reset').submit(function(event) {
        $("#dialog-confirm-reset").dialog({
          resizable: false,
          height: "auto",
          width: 300,
          modal: true,
          buttons: {
            "Reset Account": function() {
              $.post('/api/reset', function(data){
                notify(data.status, data.message);
              });
              $(this).dialog("close");
            },
            Cancel: function() {
              $(this).dialog("close");
            }
          }
        });
        event.preventDefault();
    });
    $('#name-filter').change(function(event) {
        $.post('/api/settings/save/name_filter', $(this).serialize(), function(data){
            notify(data.status, data.message);
            event.preventDefault();
        });
    });
});