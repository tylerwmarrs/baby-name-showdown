$(document).ready(function(){
    var html = '';

    var current_slide = 0;
    var i = 0;

    // keep track of current slide and all slides
    // also create dots
    var slides = $('.slide')
    $(slides).each(function() {
        if($(this).hasClass('slide-current')) {
            html += '<span class="dot dot-current"></span>';
            current_slide = i;
        } else {
            html += '<span class="dot"></span>';
        }
        i++;
    });

    $('.slide-dots').html(html);
    var dots = $('.dot');

    var remove_current_dot = function() {
        $(dots[current_slide]).removeClass('dot-current');
    };

    var remove_current_slide = function() {
        $(slides[current_slide]).removeClass('slide-current');
    };

    var slide_left = function() {
        console.log('swipe left');
        remove_current_dot();
        remove_current_slide();
        if (current_slide - 1 < 0) {
            console.log('less than');
            //current_slide = slides.length - 1;
            current_slide = 0
        } else {
            console.log('else');
            current_slide -= 1;
        }

        console.log('current slide: ' + current_slide);
        $(slides[current_slide]).addClass('slide-current');
        $(dots[current_slide]).addClass('dot-current');
    };

    var slide_right = function() {
        console.log('swipe right');
        remove_current_dot();
        remove_current_slide();
        if (current_slide + 1 > slides.length - 1) {
            console.log('greater than');
            current_slide = 0;
        } else {
            console.log('else');
            current_slide += 1;
        }

        console.log('current slide: ' + current_slide);
        $(slides[current_slide]).addClass('slide-current');
        $(dots[current_slide]).addClass('dot-current');
    };

    $(".slide-container").swipe( {
        //Generic swipe handler for all directions
        swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
            slide_left();
        },
        swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
            slide_right();
        },
        //Default is 75px, set to 0 for demo so any distance triggers swipe
         threshold:100
    });

    $('div.arrow-left').on('click', function() {
        slide_left();
    });
    
    $('div.arrow-right').on('click', function() {
        slide_right();
    });

    $("body").keydown(function(e){
        // left arrow
        if ((e.keyCode || e.which) == 37) {   
            slide_left();
        }
        // right arrow
        if ((e.keyCode || e.which) == 39) {
            slide_right();
        }   
    });
});