from playhouse.migrate import *

import os, sys

current = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(current, '..'))

from db_settings import DB
migrator = PostgresqlMigrator(DB)

# Add app_id to user table and add index
app_id = CharField(default='', index=True)

migrate(
    migrator.add_column('user', 'app_id', app_id)
)