import logging
import json
import sys
from logging.config import dictConfig

def get_logger_instance(name):
	# LOAD CONFIG
	ext_config = json.loads(open('config.json').read())
	debug = ext_config['DEBUG']

	# LOGGING
	logger = logging.getLogger(name)
	handler = logging.StreamHandler(sys.stderr)
	handler.setFormatter(logging.Formatter('%(name)s - %(levelname)s - %(message)s'))
	logger.addHandler(handler)
	if debug:
	    logger.setLevel(logging.DEBUG)

	return logger