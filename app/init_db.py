import os
import models as m
import pandas as pd

def delete_existing_db():
    try:
        os.remove(m.DB.database)
    except:
        pass

def create_db_tables():
    m.DB.create_tables([
        m.User,
        m.UserPartner,
        m.Name,
        m.UserOpinion,
        m.PartnerInvitation,
        m.UserSettings
    ])
    
def insert_names():
    df = pd.read_csv('../model/features.csv')
    with m.DB.atomic():
        for index, row in df.iterrows():
            new_name = m.Name(
                name=row['Name'],
                length=int(row['Length']),
                name_percentage_male=float(row['Name % Male']),
                name_percentage_female=float(row['Name % Female']),
                popularity_percentage=float(row['Popularity %']),
                metaphone_code=int(row['Metaphone Encoded']),
                starts_with_code=int(row['StartsWith Encoded']),
                ends_with_code=int(row['EndsWith Encoded']),
                cluster=int(row['Cluster'])
            )
            new_name.save()

def main():
    delete_existing_db()
    create_db_tables()
    insert_names()

if __name__ == '__main__':
    main()