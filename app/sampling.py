import random
from enum import Enum

import numpy as np

import models as m
import my_logger

logger = my_logger.get_logger_instance(__name__)

class BiasedWeight(Enum):
    """
    Values in which the sampler uses to bias name
    selection.
    """
    USER = 45
    RANDOM = 50
    PARTNER = 5
    
class SelectionMethod(Enum):
    """
    The method used to choose a name.
    """
    RANDOM = 1
    USER = 2
    PARTNER = 3
    
def calculate_weight(liked, total_names, bias):
    """
    Calculates the weight to use in sampling.
    """
    return (liked / total_names) * bias

def normalize(v):
    """
    0-1 normalizes an array of values.
    """
    norm=np.linalg.norm(v, ord=1)
    if norm==0:
        norm=np.finfo(v.dtype).eps
    return v/norm

def which_strata(user_liked, partner_liked, total_names):
    """
    Determines which strata sampling should be used based on
    biased weighted calculations.
    """
    if user_liked < 10:
        user_liked = 0
        
    if partner_liked < 10:
        partner_liked = 0
    
    user_weight = calculate_weight(
        user_liked, total_names, BiasedWeight.USER.value)
    partner_weight = calculate_weight(
        partner_liked, total_names, BiasedWeight.PARTNER.value)
    random_weight = calculate_weight(
        user_liked + partner_liked, total_names, 
        BiasedWeight.RANDOM.value)

    weights = np.array([
        user_weight,
        partner_weight,
        random_weight
    ])
    
    weights = normalize(weights)
    if weights[0] == 0 and weights[1] == 0:
        return SelectionMethod.RANDOM
    
    return np.random.choice([
        SelectionMethod.USER,
        SelectionMethod.PARTNER,
        SelectionMethod.RANDOM], p=weights)

def random_sample_by_names(names, user):
    """
    Randomly samples a recommended name based on filter options
    of user likes, dislikes and sex filter.
    """
    # convert names to SQL in statement format
    sql_names = []
    for name in names:
        sql_names.append("'{}'".format(name))        
    
    name = None
    
    query = m.Name.raw("""
    SELECT n.*
    FROM name n
    WHERE id NOT IN (
        SELECT up.name_id
        FROM useropinion up
        WHERE up.dislikes > 1
        and up.likes > 4
        and up.user_id = {}
    )
    {}
    AND name in ({})
    ORDER BY RANDOM() LIMIT 1;
    """.format(str(user.id), name_filter_sql(user), ','.join(sql_names)))
    for name in query:
        break
        
    return name

def name_filter_sql(user):
    """
    Helper to generate partial SQL filter for user preference
    based on sex of name.
    """
    name_pref = m.UserSettings.find_by_user(user).filter_state
    sql = ''
    if name_pref == m.NameFilterState.BOY_NAMES_ONLY.value:
        sql = 'AND n.name_percentage_male > 0'
    elif name_pref == m.NameFilterState.GIRL_NAMES_ONLY.value:
        sql = 'AND n.name_percentage_female > 0'
    
    return sql

def sample_user_recommended(user, name_table):
    query = m.Name.raw("""
    SELECT n.*
    FROM name n
    JOIN useropinion up
    ON n.id = up.name_id
    AND up.likes > 0
    AND up.dislikes < 2
    AND up.user_id = {}
    {}
    ORDER BY RANDOM() LIMIT 1;
    """.format(str(user.id), name_filter_sql(user)))
    logger.debug(query.sql())        
    for liked_name in query:
        break

    logger.debug('Found liked name to recommend on as: ' + liked_name.name)
    
    # Find a random name similar to what they liked before.
    similar_name = liked_name.name
    name = random_sample_by_names(name_table[liked_name.name], user)
    logger.debug('Similar name found as: ' + name.name)
    
    return (name, similar_name)

def sample_partner(user, partner):
    query = m.Name.raw("""
    SELECT *
    FROM name n
    WHERE id NOT IN (
        SELECT up.name_id
        FROM useropinion up
        WHERE up.dislikes > 1
        and up.likes > 4        
        and up.user_id = {}
    )
    and id IN (
        SELECT up.name_id
        FROM useropinion up
        WHERE up.likes > 0
        and up.user_id = {}
    )
    {}
    ORDER BY RANDOM() LIMIT 1;
    """.format(str(user.id), str(partner.id), name_filter_sql(user)))

    partner_liked = None
    for partner_liked in query:
        break

    name = partner_liked
    return (name, '')

def sample_random(user):
    query = m.Name.raw("""
    SELECT *
    FROM name n
    WHERE id NOT IN (
        SELECT up.name_id
        FROM useropinion up
        WHERE up.dislikes > 1
        and up.likes > 4
        and up.user_id = {}
    )
    {}
    ORDER BY RANDOM() LIMIT 1;
    """.format(str(user.id), name_filter_sql(user)))
    logger.debug(query.sql())

    for name in query:
        break

    return (name, '')

def sex_based_counts(user):
    user_settings = m.UserSettings.find_by_user(user)
    sex_pref = user_settings.filter_state
    logger.debug('user_settings filter state: ' + str(sex_pref))

    partner = user.approved_partner()
    logger.debug('has partner: ' + str(partner != None))

    like_count = 0
    partner_like_count = 0
    total_names = 0

    # Boy names only
    if sex_pref == m.NameFilterState.BOY_NAMES_ONLY.value:
        logger.debug('boy name counts')
        like_count = m.UserOpinion.user_boy_like_count(user)
        if partner:
            partner_like_count = m.UserOpinion.user_boy_like_count(partner)
        total_names = m.Name.total_boy_names()

    # Girl names only
    elif sex_pref == m.NameFilterState.GIRL_NAMES_ONLY.value:
        logger.debug('girl name counts')
        like_count = m.UserOpinion.user_girl_like_count(user)
        if partner:
            partner_like_count = m.UserOpinion.user_girl_like_count(partner)
        total_names = m.Name.total_girl_names()

    # Any name
    else:
        logger.debug('any name counts')
        like_count = m.UserOpinion.user_like_count(user)        
        if partner:
            partner_like_count = m.UserOpinion.user_like_count(partner)
        total_names = m.Name.total_names()

    logger.debug('like count: ' + str(like_count))
    logger.debug('partner_like_count: ' + str(partner_like_count))
    logger.debug('total names: ' + str(total_names))

    return (like_count, partner_like_count, total_names)

def sample_name(user, name_table):
    # TODO - eventually the user could go through every name and
    # we no longer have any names to select from. I need to handle
    # this case.
    logger.debug('Sampling Name')    
    name = None

    like_count, partner_like_count, total_names = sex_based_counts(user)
    method = which_strata(like_count, partner_like_count, total_names)

    if method == SelectionMethod.USER:
        logger.debug('Sampling method is USER')
        name, similar_name = sample_user_recommended(user, name_table)
    elif method == SelectionMethod.PARTNER:
        logger.debug('Sampling method is PARTNER')
        partner = user.approved_partner()
        name, similar_name = sample_partner(user, partner)
        
    if name is None or method == SelectionMethod.RANDOM:
        logger.debug('Sampling method is RANDOM')
        name, similar_name = sample_random(user)
        
    return (name, similar_name)